export { default as LindsethOgFar } from "./lindsethogfar.jpg";
export { default as Logo } from "./logo.svg";
export { default as Salgsbod } from "./salgsbod_leirnes.jpg";
export { default as HonningGlass } from "./honningglass.jpg";
export { default as StillBilde } from "./stillbilde.jpg";
export { default as FrodeISverm } from "./frode_i_sverm.mp4";
