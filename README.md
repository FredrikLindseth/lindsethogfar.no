# Lindseth og far .no

![logo](src/assets/logo.svg)

Alt er laget i [Gatsby.js](https://www.gatsbyjs.com/docs/quick-start/)

## Oppsett lokalt

Innstaller det nødvendige

```sh
sudo apt-get install nodejs npm
sudo npm install -g gatsby-cli
```

og så for å kjøre opp development-bygg av gatsy

```sh
gatsby develop
```

Gå til [localhost:8000](http://localhost:8000/) for å se siden

## webserver

nginx vhost : [lindsethogfar.no](./lindsethogfar.no)

vhost root :  `/var/www/lindsethogfar.no/`

Symlink denne vhosten over til `sites-enabled`

```sh
sudo ln -s /home/fredrik/www/lindsethogfar.no/lindsethogfar.no /etc/nginx/sites-enabled/lindsethogfar.no
```

Symlink `/src/` til webserveren-rooten

```sh
sudo ln -s /home/fredrik/www/lindsethogfar.no/public/ /var/www/lindsethogfar.no
```

For å deploye er det bare å

```sh
git pull
npm i
gatsby build
```

## Nginx vhost

```nginx
server {
        root /var/www/lindsethogfar;
        index index.html ;
        server_name lindsethogfar.no www.lindsethogfar.no;
        location / {
                try_files $uri $uri/ =404;
        }
}
```

## Fremtidig innhold på nettsiden

- Galleri

### FAQ

- Voks i honning
- Svarte prikker i honning
- Krystallisert honning
- En sverm

- Varmebehandling, annen behandling
- Falsk honning
- Holdbarhet ref merkevareforskriften
- Kjøpe voks og voksmangel
- er honning medisin / sårbehandling
